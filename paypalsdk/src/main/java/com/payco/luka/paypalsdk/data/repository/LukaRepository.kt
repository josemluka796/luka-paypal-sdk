package com.payco.luka.paypalsdk.data.repository

import com.payco.luka.paypalsdk.Luka
import com.payco.luka.paypalsdk.data.datasources.LukaDataSourceFactory
import com.payco.luka.paypalsdk.data.models.LukaAuthCredentials
import com.payco.luka.paypalsdk.data.models.LukaPaymentResult
import com.payco.luka.paypalsdk.data.models.PaypalTransaction
import com.payco.luka.paypalsdk.utils.DataState
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class LukaRepository @Inject constructor(
    private val lukaDataSourceFactory: LukaDataSourceFactory
) {
    suspend fun login(credentials: LukaAuthCredentials): Flow<DataState<*>> =
        flow<DataState<*>> {
            lukaDataSourceFactory.login(credentials)
//            Luka.token = response.headers()["token"] ?: ""
//            Luka.id = response.headers()["id"] ?: ""
//            emit(DataState.Success(true))
                .suspendOnSuccess {
                    Luka.token = headers[LUKA_TOKEN] ?: ""
                    Luka.id = headers[LUKA_ID] ?: ""
                    Luka.clientId = headers[PAYPAL_CLIENT_ID] ?: ""
                    emit(DataState.Success(data))
                }
                .suspendOnError {
                    emit(DataState.TypedError(statusCode))
                }.suspendOnException {
                    this.exception.printStackTrace()
                    emit(DataState.Exception(this.exception))
                }

        }.flowOn(Dispatchers.IO)

    suspend fun reportTransaction(paypalTransaction: PaypalTransaction) : Flow<DataState<LukaPaymentResult>> =
        flow {
            lukaDataSourceFactory

                .reportTransaction(paypalTransaction)
                .suspendOnSuccess {
                    emit(DataState.Success(data))
                }
                .suspendOnError {
                    emit(DataState.TypedError(statusCode))
                }.suspendOnException {
                    this.exception.printStackTrace()
                    emit(DataState.Exception(this.exception))
                }
        }

    companion object {
        const val LUKA_TOKEN = "token"
        const val LUKA_ID = "id"
        const val PAYPAL_CLIENT_ID = "paypal-client-id"
    }
}