package com.payco.luka.paypalsdk.di

import com.google.gson.GsonBuilder
import com.payco.luka.BuildConfig
import com.payco.luka.paypalsdk.Luka
import com.payco.luka.paypalsdk.data.api.LukaApiInterface
import com.payco.luka.paypalsdk.data.datasources.LukaDataSourceFactory
import com.payco.luka.paypalsdk.data.datasources.LukaRemoteDataSource
import com.payco.luka.paypalsdk.data.repository.LukaRepository
import com.payco.luka.paypalsdk.domain.usecase.AuthLukaUseCase
import com.payco.luka.paypalsdk.domain.usecase.LukaUseCaseFactory
import com.payco.luka.paypalsdk.domain.usecase.ReportTransactionUseCase
import com.skydoves.sandwich.adapters.ApiResponseCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class LukaModule {

    @Provides
    fun getLukaHttpClient() : OkHttpClient {
        println("Providing Http Client")
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .addInterceptor {
                it.proceed(
                    request =
                        it.request().newBuilder().apply {
                            if (Luka.token.isNotEmpty())
                                this.addHeader("Authorization", "Bearer ${Luka.token}")
                        }.build()
                )
            }
            .readTimeout(40, TimeUnit.MINUTES)
            .connectTimeout(40, TimeUnit.MINUTES)
            .callTimeout(10, TimeUnit.MINUTES)
            .writeTimeout(10, TimeUnit.MINUTES)
            .build()
    }

    @Provides
    fun getLukaApiInterface(retrofit: Retrofit) : LukaApiInterface =
        retrofit.create(LukaApiInterface::class.java)

    @Provides
    fun getRetrofit(httpClient: OkHttpClient) =
        Retrofit.Builder()
            .addCallAdapterFactory(ApiResponseCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(
                GsonBuilder()
                    .setLenient()
                    .create()
            ))
            .client(httpClient)
            .baseUrl(BuildConfig.API_URL)
            .build()

    @Provides
    fun getAuthLukeUseCase(repository: LukaRepository) =
        AuthLukaUseCase(repository)

    @Provides
    fun getReportTransactionUseCase(repository: LukaRepository) =
        ReportTransactionUseCase(repository)

    @Provides
    fun getLukaUseCaseFactory(
        authLukaUseCase: AuthLukaUseCase,
        reportTransactionUseCase: ReportTransactionUseCase
    ) = LukaUseCaseFactory(authLukaUseCase,reportTransactionUseCase)

    @Provides
    fun getLukaRepository(dataSource: LukaDataSourceFactory) =
        LukaRepository(dataSource)

    @Provides
    fun getLukaDataSource(lukaApiInterface: LukaApiInterface) =
        LukaRemoteDataSource(lukaApiInterface)

}