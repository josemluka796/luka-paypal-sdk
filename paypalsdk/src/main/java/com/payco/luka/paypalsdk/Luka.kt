package com.payco.luka.paypalsdk

import com.payco.luka.paypalsdk.di.DaggerWrapper
import com.payco.luka.paypalsdk.utils.LukaConfig
import javax.inject.Inject

open class Luka {
    /**
     * Api object
     */
    @Inject internal lateinit var paypal: PayPalApi
    @Inject internal lateinit var lukapi: LukaApi
//    /**
//     * Creates a new [LukaApi] instance
//     */
//    open fun onCreateBuilder(context: Context): LukaApiBuilder {
//        return LukaApiBuilder(context)
//    }
//
//    /**
//     * Resolves the singleton instance of [LukaApi]
//     */
//    internal fun resolveApi(
//        context: Context,
//        credentials: LukaAuthCredentials,
//        setup: ((LukaApiBuilder) -> Unit)? = null
//    ): LukaApi {
//        if (!::api.isInitialized) {
//            val appContext = context.applicationContext
//
//            val builder = onCreateBuilder(appContext)
//            setup?.invoke(builder)
//
//            api = builder.build().apply {
//                authenticate(credentials)
//            }
//
//        }
//
//        return api
//    }
//

    companion object {

        @JvmStatic
        var token: String = ""
        @JvmStatic
        var id: String = ""
        @JvmStatic
        var clientId: String = ""
//        internal const val API_URL = "https://lukaapi.payco.net.ve/api/v1/"
//        internal const val TEST_API_URL = "https://bspaycoapi-qa.payco.net.ve/api/v1/"
////        internal const val TEST_API_URL = "https://197a-45-230-169-73.ngrok.io/api/v1/"
//        internal const val DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
//
//        /**
//         * Keeps the singleton [LukaApi] instance
//         */
        internal lateinit var sInstance: Luka

        @JvmStatic fun initialize(config: LukaConfig = LukaConfig.default()): Luka {
            return initializer {
                Luka().apply {
                    DaggerWrapper.component?.inject(this)
                    this.lukapi.init(config)
                }
            }
        }

        private fun initializer(func: () -> Luka): Luka {
            if (!::sInstance.isInitialized) {
                sInstance = func.invoke()
            }

            return sInstance
        }
//
        @JvmStatic fun paypal(): PayPalApi {
            if (!::sInstance.isInitialized) {
                throw RuntimeException("Luka is not initialized")
            }

            return sInstance.paypal
        }

        @JvmStatic fun luka(): LukaApi {
            if (!::sInstance.isInitialized) {
                throw RuntimeException("Luka is not initialized")
            }

            return sInstance.lukapi
        }

    }
}