package com.payco.luka.paypalsdk.data.api

import com.payco.luka.paypalsdk.data.models.LukaAuthCredentials
import com.payco.luka.paypalsdk.data.models.LukaPaymentResult
import com.payco.luka.paypalsdk.data.models.PaypalTransaction
import com.skydoves.sandwich.ApiResponse
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Luka Auth services
 */
interface LukaApiInterface {

    /**
     * Service to login to Luka. This responds with the authorization token
     * @see LukaAuthCredentials
     */
    @POST("servicio/login")
    @Headers("GetPaypalClientId: true")
    suspend fun login(@Body body: LukaAuthCredentials): ApiResponse<ResponseBody>

    @POST("transaccion/paypal.capture.order")
    suspend fun reportTransaction(@Body body: PaypalTransaction): ApiResponse<LukaPaymentResult>
}