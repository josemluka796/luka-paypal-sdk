package com.payco.luka.paypalsdk.utils

sealed class DataState<out R> {
    data class Success<out T>(val data: T) : DataState<T>()
    data class Exception(val exception: Throwable) : DataState<Nothing>()
    object Loading : DataState<Nothing>()
    data class Error(val error: String): DataState<Nothing>()
    data class TypedError<out T>(val error: T): DataState<Nothing>()
}