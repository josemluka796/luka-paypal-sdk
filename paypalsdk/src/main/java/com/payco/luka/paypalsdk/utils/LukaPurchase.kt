package com.payco.luka.paypalsdk.utils

class LukaPurchase(
    val name: String,
    val description: String,
    val amount: Double,
    val qty: Int = 1
)