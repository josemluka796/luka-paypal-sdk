package com.payco.luka.paypalsdk.data.models

import com.google.gson.annotations.SerializedName
import okhttp3.Credentials

/**
 * Credentials to login into Luka Services using [Auth.login] endpoint.
 */
class LukaAuthCredentials(
    /**
     * Username provided by Luka
     */
    @SerializedName("Username")
    var user: String = "",

    /**
     * Password provided by Luka
     */
    @SerializedName("Password")
    var password: String = "",

    var packageName: String = ""
) {
    internal val basic: String
        get() = Credentials.basic(user, password)

    internal val returnUrl : String
        get() = "${packageName}://paypalpay"

}