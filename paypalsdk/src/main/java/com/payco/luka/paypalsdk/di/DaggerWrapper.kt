package com.payco.luka.paypalsdk.di

object DaggerWrapper {
    private var mComponent: LukaComponent? = null
    val component: LukaComponent?
        get() {
            if (mComponent == null) {
                initComponent()
            }
            return mComponent
        }

    private fun initComponent() {
        mComponent = DaggerLukaComponent
            .builder()
            .lukaModule(LukaModule())
            .build()
    }
}