package com.payco.luka.paypalsdk.data.datasources

import com.payco.luka.paypalsdk.data.api.LukaApiInterface
import com.payco.luka.paypalsdk.data.models.LukaAuthCredentials
import com.payco.luka.paypalsdk.data.models.LukaPaymentResult
import com.payco.luka.paypalsdk.data.models.PaypalTransaction
import com.skydoves.sandwich.ApiResponse
import okhttp3.ResponseBody
import javax.inject.Inject

class LukaRemoteDataSource @Inject constructor(private val api: LukaApiInterface) {

    suspend fun login(creds: LukaAuthCredentials) : ApiResponse<ResponseBody> =
        api.login(creds)

    suspend fun reportTransaction(paypalTransaction: PaypalTransaction): ApiResponse<LukaPaymentResult> =
        api.reportTransaction(paypalTransaction)
}