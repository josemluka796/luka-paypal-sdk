package com.payco.luka.paypalsdk.domain.usecase

import com.payco.luka.paypalsdk.data.models.LukaAuthCredentials
import com.payco.luka.paypalsdk.data.repository.LukaRepository
import com.payco.luka.paypalsdk.utils.DataState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AuthLukaUseCase @Inject constructor(private val lukaRepository: LukaRepository) {
    suspend operator fun invoke(creds: LukaAuthCredentials) : Flow<DataState<*>> =
        lukaRepository.login(creds)
}