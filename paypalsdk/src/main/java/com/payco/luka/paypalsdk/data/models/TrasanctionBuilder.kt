package com.payco.luka.paypalsdk.data.models

class TransactionBuilder {

    var loading: (() -> Unit)? = null
    var success: ((LukaResult) -> Unit) ? = null
    var error : ((LukaResult) -> Unit) ? = null
    var failed: ((LukaResult) -> Unit) ? = null
    var execCall : (() -> Unit )? = null

    fun onLoading(call: () -> Unit ) : TransactionBuilder {
        loading = call
        return this
    }

    fun onSuccess(call: (LukaResult) -> Unit ) : TransactionBuilder {
        success = call
        return this
    }

    fun onFailed(call: ((LukaResult) -> Unit)) : TransactionBuilder {
        failed = call
        return this
    }

    fun onError(call: (LukaResult) -> Unit) : TransactionBuilder {
        error = call
        return this
    }

    fun onExec(call: () -> Unit) : TransactionBuilder{
        execCall = call
        return this
    }

    fun exec() {
        execCall?.invoke()
    }



}