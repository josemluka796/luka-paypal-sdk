package com.payco.luka.paypalsdk.di

import com.payco.luka.paypalsdk.Luka
import com.payco.luka.paypalsdk.PayPalApi
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [LukaModule::class])
interface LukaComponent {
    fun inject(luka: Luka)
    fun getPaypalApi(): PayPalApi
    fun getLukaApi(): PayPalApi
}