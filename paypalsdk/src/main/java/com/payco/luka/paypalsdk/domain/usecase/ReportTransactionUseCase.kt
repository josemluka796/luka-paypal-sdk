package com.payco.luka.paypalsdk.domain.usecase

import com.payco.luka.paypalsdk.data.models.LukaPaymentResult
import com.payco.luka.paypalsdk.data.models.PaypalTransaction
import com.payco.luka.paypalsdk.data.repository.LukaRepository
import com.payco.luka.paypalsdk.utils.DataState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ReportTransactionUseCase @Inject constructor(private val lukaRepository: LukaRepository) {

    suspend operator fun invoke(trx: PaypalTransaction) : Flow<DataState<LukaPaymentResult>> =
        lukaRepository.reportTransaction(trx)
}