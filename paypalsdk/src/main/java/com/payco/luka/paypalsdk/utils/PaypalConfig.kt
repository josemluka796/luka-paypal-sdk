package com.payco.luka.paypalsdk.utils

class PaypalConfig(
    val environment: LukaEnvironment,
    val currency: LukaCurrency,
    val loggingEnabled: Boolean
) {

    class Builder {
        var environment: LukaEnvironment = LukaEnvironment.LIVE
        var currency: LukaCurrency = LukaCurrency.USD
        var loggingEnabled: Boolean = false


        fun isLoggingEnabled(enabled: Boolean) : Builder {
            loggingEnabled = enabled
            return this
        }

        fun setEnvironment(newEnv : LukaEnvironment) : Builder {
            environment = newEnv
            return this
        }

        fun setCurrency(currency: LukaCurrency) : Builder {
            this.currency = currency
            return this
        }

        fun build() : PaypalConfig {
            return PaypalConfig(
                environment,
                currency,
                loggingEnabled
            )
        }
    }

    companion object {
        fun default(): PaypalConfig {
            return PaypalConfig(
                environment = LukaEnvironment.SANDBOX,
                currency = LukaCurrency.USD,
                loggingEnabled = true
            )
        }
    }

}


