package com.payco.luka.paypalsdk.data.models

import com.google.gson.annotations.SerializedName
import com.payco.luka.paypalsdk.utils.LukaCurrency

data class PaypalTransaction(
    @SerializedName("OrderId")
    val orderId: String? ,
    @SerializedName("Monto")
    val amount: Double,
    @SerializedName("Link")
    val link: String? = null,
    @SerializedName("MontoOriginal")
    val initAmount: Double? = null,
    @SerializedName("CargosAdicionales")
    val additionalCharges: Double? = null,
    @SerializedName("Email")
    val email: String,
    @SerializedName("IdCanal")
    val channelId: Int = 4,
    @SerializedName("IdTraza")
    var merchantId: String = "",
    @SerializedName("Moneda")
    var currency: LukaCurrency = LukaCurrency.USD
)