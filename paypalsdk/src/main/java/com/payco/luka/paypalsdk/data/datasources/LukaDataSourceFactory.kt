package com.payco.luka.paypalsdk.data.datasources

import com.payco.luka.paypalsdk.data.models.LukaAuthCredentials
import com.payco.luka.paypalsdk.data.models.LukaPaymentResult
import com.payco.luka.paypalsdk.data.models.PaypalTransaction
import com.skydoves.sandwich.ApiResponse
import okhttp3.ResponseBody
import java.util.*
import javax.inject.Inject

class LukaDataSourceFactory @Inject constructor(
    private val remote: LukaRemoteDataSource
) {
    suspend fun login(creds: LukaAuthCredentials) : ApiResponse<ResponseBody> = remote.login(creds)
    suspend fun reportTransaction(transaction: PaypalTransaction) : ApiResponse<LukaPaymentResult> = remote.reportTransaction(transaction)

    private val callbackList: Deque<suspend () -> ApiResponse<*>> = ArrayDeque()

//    suspend fun request( callback: suspend () -> ApiResponse<*> ) : LukaDataSourceFactory {
//        callbackList.add(callback)
//        return this
//    }
//
//    suspend fun execute() : SharedFlow<>{
//        for (i in 0..callbackList.size) {
//            val callback = callbackList.pop()
//            callback.invoke()
//        }
//    }
}