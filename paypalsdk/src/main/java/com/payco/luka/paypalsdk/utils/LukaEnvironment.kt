package com.payco.luka.paypalsdk.utils

import com.paypal.checkout.config.Environment

enum class LukaEnvironment {
    LIVE,
    SANDBOX,
    LOCAL;

    internal fun map() : Environment =
        when (this) {
            LIVE -> Environment.LIVE
            SANDBOX -> Environment.SANDBOX
            LOCAL -> Environment.LOCAL
            else -> Environment.STAGE
        }
}
