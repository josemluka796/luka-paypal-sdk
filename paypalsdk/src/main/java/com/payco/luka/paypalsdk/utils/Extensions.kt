package com.payco.luka.paypalsdk.utils

import kotlinx.coroutines.flow.Flow

suspend  fun <T> Flow<DataState<T>>.then(
    onError : (suspend () -> Unit)? = null,
    onSuccess: suspend (data: T) -> Unit
) {
    this.collect {
        when(it) {
            is DataState.Success -> {
                onSuccess.invoke(it.data)
            }

            is DataState.TypedError<*> -> {
                onError?.invoke()
            }
        }
    }
}
