package com.payco.luka.paypalsdk.domain.usecase

import com.payco.luka.paypalsdk.data.models.LukaAuthCredentials
import com.payco.luka.paypalsdk.data.models.LukaPaymentResult
import com.payco.luka.paypalsdk.data.models.PaypalTransaction
import com.payco.luka.paypalsdk.utils.DataState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LukaUseCaseFactory @Inject constructor(
    private val authLukaUseCase: AuthLukaUseCase,
    private val reportTransactionUseCase: ReportTransactionUseCase
) {

    suspend fun login(credentials: LukaAuthCredentials) : Flow<DataState<*>> =
        authLukaUseCase(credentials)

    suspend fun reportTransaction(paypalTransaction: PaypalTransaction) : Flow<DataState<LukaPaymentResult>> =
        reportTransactionUseCase(paypalTransaction)
}