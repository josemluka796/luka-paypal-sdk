package com.payco.luka.paypalsdk.utils

import com.payco.luka.paypalsdk.data.models.LukaAuthCredentials

class LukaConfig(
    var auth: LukaAuthCredentials
) {
    class Builder {
        var creds: LukaAuthCredentials = LukaAuthCredentials()

        fun setCredentials(credentials: LukaAuthCredentials) : Builder {
            creds = credentials
            return this
        }

        fun build() : LukaConfig {
            return LukaConfig(creds)
        }
    }

    companion object {
        fun default() : LukaConfig {
            return LukaConfig.Builder()
                .setCredentials(LukaAuthCredentials())
                .build()
        }
    }
}