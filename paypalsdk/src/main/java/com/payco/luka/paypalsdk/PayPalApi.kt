package com.payco.luka.paypalsdk

import android.app.Application
import android.content.Context
import com.payco.luka.paypalsdk.data.models.*
import com.payco.luka.paypalsdk.domain.usecase.LukaUseCaseFactory
import com.payco.luka.paypalsdk.utils.LukaPurchase
import com.payco.luka.paypalsdk.utils.PaypalConfig
import com.payco.luka.paypalsdk.utils.then
import com.paypal.checkout.PayPalCheckout
import com.paypal.checkout.approve.OnApprove
import com.paypal.checkout.cancel.OnCancel
import com.paypal.checkout.config.CheckoutConfig
import com.paypal.checkout.config.SettingsConfig
import com.paypal.checkout.config.UIConfig
import com.paypal.checkout.createorder.CreateOrder
import com.paypal.checkout.createorder.CurrencyCode
import com.paypal.checkout.createorder.OrderIntent
import com.paypal.checkout.createorder.UserAction
import com.paypal.checkout.error.OnError
import com.paypal.checkout.order.*
import com.paypal.pyplcheckout.sca.runOnUiThread
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

open class PayPalApi @Inject constructor(
    private val lukaUseCaseFactory: LukaUseCaseFactory
) {

    private lateinit var transactionBuilder: TransactionBuilder
    private lateinit var config : PaypalConfig

    fun init(context: Context, configuration : PaypalConfig) {
        config = configuration
        initSdk(context)
    }

    private fun initSdk(context: Context) {
        CoroutineScope(Dispatchers.Main).launch {
            LukaApi.loginState.collect {
                if (it) {
                    setUpPaypalConfig(context)
                    registerPaypalCallbacks()
                }
            }
        }
    }

    private var purchase: LukaPurchase? = null

    fun checkout(purchaseItem: LukaPurchase) : TransactionBuilder {
        this.purchase = purchaseItem
        transactionBuilder = TransactionBuilder()
            .onExec {
                transactionBuilder.loading?.invoke()
                beginPaypalTransaction()
            }

        return transactionBuilder
    }

    private fun proceedToCheckout()  {
        purchase?.let {
            PayPalCheckout.startCheckout(
                CreateOrder { createOrderActions ->
                    val order = Order(
                        intent = OrderIntent.CAPTURE,
                        appContext = AppContext(
                            userAction = UserAction.PAY_NOW
                        ),
                        purchaseUnitList = listOf(
                            PurchaseUnit(

                                amount = Amount(
                                    currencyCode = config.currency.map(),
                                    value = it.amount.toString()
                                ),
                            )
                        )
                    )
                    createOrderActions.create(order)
                }
            )
        }

    }

    private fun setUpPaypalConfig(context: Context) {
        val config = CheckoutConfig(
            application = context as Application,
            clientId = Luka.clientId,
            environment = config.environment.map(),
            currencyCode = CurrencyCode.USD,
            userAction = UserAction.PAY_NOW,
            settingsConfig = SettingsConfig(config.loggingEnabled),
            uiConfig = UIConfig(false),
            returnUrl = LukaApi.config.auth.returnUrl
        )

        PayPalCheckout.setConfig(config)
    }

    private fun registerPaypalCallbacks() {
        PayPalCheckout.registerCallbacks(
            onApprove = OnApprove { approval ->
                reportTransactionToLuka(
                    PaypalTransaction(
                        orderId = approval.data.orderId,
                        amount = purchase?.amount ?: 0.0,
                        email = approval.data.payer?.email?.stringValue ?: "",
                        merchantId = Luka.id
                    )
                )
            },
            onCancel = OnCancel {
                // Optional callback for when a buyer cancels the paysheet
                transactionBuilder.failed?.invoke(LukaResult(false, 1002))
            },
            onError = OnError { errorInfo ->
                errorInfo.error.printStackTrace()
                transactionBuilder.error?.invoke(LukaResult(false, 1003))
            }
        )
    }


    private fun beginPaypalTransaction() {
        CoroutineScope(Dispatchers.Main).launch {
            LukaApi.loginState.collect {
                if (it) proceedToCheckout()
            }
        }
    }



    private fun reportTransactionToLuka(paypalTransaction: PaypalTransaction) {
        CoroutineScope(Dispatchers.Main).launch {
            LukaApi.loginState.flatMapConcat { isLoggedIn ->
                if (isLoggedIn)
                    return@flatMapConcat lukaUseCaseFactory.reportTransaction(paypalTransaction)
                emptyFlow()
            }.then(
                onError =  {
                    transactionBuilder.error?.invoke(LukaResult(false, 1000))
                }
            ) {
                runOnUiThread {
                    if (it.success) transactionBuilder.success?.invoke(LukaResult(it.success, 0))
                    else transactionBuilder.error?.invoke(LukaResult(false, 1001))
                }
            }
        }

    }

}