package com.payco.luka.paypalsdk.data.models

import com.google.gson.annotations.SerializedName
import com.payco.luka.paypalsdk.utils.LukaCurrency

class LukaPaymentResult(
    @SerializedName("MerchantId")
    val merchantId: String?,
    @SerializedName("Descripcion")
    val description: String?,
    @SerializedName("TrazaId")
    val traceId: String?,
    @SerializedName("Exitoso")
    val success: Boolean = false,
    @SerializedName("MedioDePago")
    val paymentMethod: String,
    @SerializedName("Moneda")
    val currency: LukaCurrency?
)