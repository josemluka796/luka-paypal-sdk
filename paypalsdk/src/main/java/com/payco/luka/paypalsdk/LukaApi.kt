package com.payco.luka.paypalsdk

import com.payco.luka.paypalsdk.domain.usecase.LukaUseCaseFactory
import com.payco.luka.paypalsdk.utils.LukaConfig
import com.payco.luka.paypalsdk.utils.then
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class LukaApi @Inject constructor(
    private val lukaUseCaseFactory: LukaUseCaseFactory
) {

    fun init(configuration: LukaConfig) {
        config = configuration
        // make a login
        login()
    }

    private fun login() =
        CoroutineScope(Dispatchers.IO).launch {
            lukaUseCaseFactory.login(config.auth).then {
                _loginState.value = true
            }
        }


    companion object  {
        private val _loginState : MutableStateFlow<Boolean> = MutableStateFlow(false)
        val loginState : StateFlow<Boolean> = _loginState
        lateinit var config: LukaConfig
    }

}