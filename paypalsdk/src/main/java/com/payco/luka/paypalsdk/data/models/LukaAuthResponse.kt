package com.payco.luka.paypalsdk.data.models

import com.google.gson.annotations.SerializedName

data class LukaAuthResponse(
    @SerializedName("token")
    val token: String,
    @SerializedName("id")
    val id: String
)