package com.payco.luka.paypal

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.payco.luka.paypalsdk.Luka
import com.payco.luka.paypalsdk.LukaApi
import com.payco.luka.paypalsdk.data.models.LukaAuthCredentials
import com.payco.luka.paypalsdk.data.models.LukaResult
import com.payco.luka.paypalsdk.utils.LukaConfig
import com.payco.luka.paypalsdk.utils.LukaEnvironment
import com.payco.luka.paypalsdk.utils.PaypalConfig
import kotlinx.coroutines.awaitAll
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.ThreadLocalRandom

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class LukaApiTest {
    fun login() {
        Luka.initialize(
            LukaConfig.Builder()
                .setCredentials(
                    LukaAuthCredentials(
                        user = "ridery",
                        password = "short-rope-northern-quickly",
                        packageName = "com.payco.luka"
                    )
                ).build()
        )

        Thread.sleep(2000)
    }



    @Test
    fun paypalInit() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext

        Luka.initialize(
            LukaConfig.Builder()
                .setCredentials(
                    LukaAuthCredentials(
                        user = "ridery",
                        password = "short-rope-northern-quickly",
                        packageName = "com.payco.luka"
                    )
                ).build()
        )

        Luka.paypal().init(appContext, PaypalConfig.Builder().setEnvironment(LukaEnvironment.SANDBOX).build())
        Thread.sleep(2000)
//
//        Luka().resolveApi(appContext).apply {
//            val credentials = LukaAuthCredentials(
//                user = "ridery",
//                password = "short-rope-northern-quickly"
//            )
//            val response = lukaServiceProvider.auth().login(credentials).launchAndWait()
//
//            assertNotNull(response)
//        }

        //assertEquals("com.payco.sdk.test", appContext.packageName)
    }

    @Test
    fun chargeAmount()  {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext

        Luka.initialize(
            LukaConfig.Builder()
                .setCredentials(
                    LukaAuthCredentials(
                        user = "ridery",
                        password = "short-rope-northern-quickly",
                        packageName = "com.payco.luka"
                    )
                ).build()
        )

        Luka.paypal().init(appContext, PaypalConfig.Builder().setEnvironment(LukaEnvironment.SANDBOX).build())
        Luka.paypal()
            .chargeAmount(20.0)
            .onLoading {
                Log.i("LukaAuthTest", "chargeAmount: starting test")
            }
            .onSuccess {
                assert(it.success)
                Log.i("LukaAuthTest", "chargeAmount: successful test")
            }
            .onError {
                Log.i("LukaAuthTest", "chargeAmount: error test")
            }
            .exec()

        Thread.sleep(5000)

    }
}