package com.payco.luka_app.ui.home.domain.usecases

import com.payco.luka_app.ui.home.data.repository.HomeRepository
import javax.inject.Inject

class GetTicketsUseCase @Inject constructor(private val homeRepository: HomeRepository) {

    operator fun invoke() =
        homeRepository.getTickets()
}