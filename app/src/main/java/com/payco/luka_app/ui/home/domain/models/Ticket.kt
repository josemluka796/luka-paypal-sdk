package com.payco.luka_app.ui.home.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Ticket(
    val title:String,
    val date: String,
    val place: String,
    val quantity: Int,
    val image_url: String
) : Parcelable