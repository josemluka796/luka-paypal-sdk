package com.payco.luka_app.ui.home.data.repository

import com.payco.luka_app.ui.home.domain.models.Ticket


interface HomeRepository {
    fun getTickets() : List<Ticket>
}