package com.payco.luka_app.ui.home.domain.usecases

import com.payco.luka_app.ui.home.domain.models.Ticket


interface HomeUseCaseFactory {
    fun getTickets() : List<Ticket>
}