package com.payco.luka_app.ui.home.presentation.viewmodel

import androidx.lifecycle.ViewModel
import com.payco.luka_app.ui.home.domain.models.Ticket
import com.payco.luka_app.ui.home.domain.usecases.HomeUseCaseFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

@HiltViewModel
class HomeViewModel
    @Inject constructor(
        private val homeUseCaseFactory: HomeUseCaseFactory
    ) : ViewModel() {

    val _homeState : MutableStateFlow<HomeUiState> = MutableStateFlow(HomeUiState.Empty)
    val homeState : StateFlow<HomeUiState> = _homeState

    init {
        getTickets()
    }

    sealed class HomeUiState {
        class  ShowTickets(val tickets: List<Ticket>): HomeUiState()
        object Empty : HomeUiState()
    }



    fun getTickets() {
        val tickets = homeUseCaseFactory.getTickets()
        _homeState.value = HomeUiState.ShowTickets(tickets)
    }
}