package com.payco.luka_app.ui.home.domain.usecases

import com.payco.luka_app.ui.home.domain.models.Ticket
import javax.inject.Inject

class HomeUseCaseFactoryImpl @Inject constructor(private val getTicketsUseCase: GetTicketsUseCase):
    HomeUseCaseFactory {
    override fun getTickets() : List<Ticket> =
        getTicketsUseCase()
}