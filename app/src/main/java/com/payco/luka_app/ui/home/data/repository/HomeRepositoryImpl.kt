package com.payco.luka_app.ui.home.data.repository

import com.payco.luka_app.ui.home.data.datasources.HomeDataSourceFactory
import com.payco.luka_app.ui.home.domain.models.Ticket
import javax.inject.Inject

class HomeRepositoryImpl @Inject constructor(private val dataSourceFactory: HomeDataSourceFactory) :
    HomeRepository {

    override fun getTickets(): List<Ticket> =
        dataSourceFactory.getLocal().getTickets()

}
