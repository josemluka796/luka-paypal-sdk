package com.payco.luka_app.ui.checkout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.payco.luka.paypalsdk.Luka
import com.payco.luka.paypalsdk.utils.LukaPurchase
import com.payco.luka_app.databinding.CheckoutBottomSheetBinding
import com.payco.luka_app.ui.ticket.Offer
import com.payco.luka_app.utils.loadRect
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CheckoutBottomSheet(val offer: Offer) : BottomSheetDialogFragment() {

    private var _binding: CheckoutBottomSheetBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = CheckoutBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.description.text = offer.description
        binding.price.text = "$ ${offer.price}"
        binding.ticketImage.loadRect(offer.image_url)
        binding.buy.setOnClickListener {

            Luka.paypal().checkout(LukaPurchase("Offer", offer.description, offer.price))
                .onSuccess { result ->
                    binding.buy.isEnabled = true
                    binding.loader.visibility = View.GONE
                    Toast.makeText(requireContext(), "Successfully bought this item", Toast.LENGTH_SHORT).show()
                    dismiss()
                }.onLoading {
                    binding.loader.visibility = View.VISIBLE
                    binding.buy.isEnabled = false
                }.onError {
                    binding.loader.visibility = View.GONE
                    binding.buy.isEnabled = true
                    Toast.makeText(requireContext(), "Error ocurred during checkout for this item", Toast.LENGTH_SHORT).show()
                }.onFailed {
                    binding.loader.visibility = View.GONE
                    binding.buy.isEnabled = true
                    Toast.makeText(requireContext(), "Purchase canceled by user", Toast.LENGTH_SHORT).show()
                }
                .exec()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}