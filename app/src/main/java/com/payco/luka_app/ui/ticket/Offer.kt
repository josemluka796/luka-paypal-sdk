package com.payco.luka_app.ui.ticket

class Offer(
    val image_url: String,
    val description: String,
    val price: Double
)