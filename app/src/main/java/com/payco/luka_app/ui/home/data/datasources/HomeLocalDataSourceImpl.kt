package com.payco.luka_app.ui.home.data.datasources

import com.payco.luka_app.ui.home.domain.models.Ticket


class HomeLocalDataSourceImpl : HomeLocalDataSource {
    override fun getTickets(): List<Ticket> {
        return listOf(
            Ticket(
                "Cultura Profetica",
                "2022/04/29T00:00:00.000Z",
                "Terraza CCCT",
                2,
                "https://i.postimg.cc/wjfF4tq6/Screen-Shot-2022-04-26-at-08-30-2.png"
            ),
            Ticket(
                "Cultura Profetica",
                "2022/04/29T00:00:00.000Z",
                "Terraza CCCT",
                2,
                "https://i.postimg.cc/wjfF4tq6/Screen-Shot-2022-04-26-at-08-30-2.png"
            ),
            Ticket(
                "Cultura Profetica",
                "2022/04/29T00:00:00.000Z",
                "Terraza CCCT",
                2,
                "https://i.postimg.cc/wjfF4tq6/Screen-Shot-2022-04-26-at-08-30-2.png"
            ),
            Ticket(
                "Cultura Profetica",
                "2022/04/29T00:00:00.000Z",
                "Terraza CCCT",
                2,
                "https://i.postimg.cc/wjfF4tq6/Screen-Shot-2022-04-26-at-08-30-2.png"
            )
        )
    }
}