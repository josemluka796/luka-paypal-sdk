package com.payco.luka_app.ui.home.data.datasources

interface HomeDataSourceFactory {
    fun getLocal() : HomeLocalDataSource
}