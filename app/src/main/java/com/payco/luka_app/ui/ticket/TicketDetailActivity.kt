package com.payco.luka_app.ui.ticket

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.payco.luka_app.databinding.ActivityTicketDetailBinding
import com.payco.luka_app.ui.checkout.CheckoutBottomSheet
import com.payco.luka_app.ui.home.domain.models.Ticket
import com.payco.luka_app.utils.loadRect
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TicketDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTicketDetailBinding
    private lateinit var adapter: OfferAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTicketDetailBinding.inflate(layoutInflater).also {
            setContentView(it.root)
        }

        intent.extras?.getParcelable<Ticket>("ticket")?.run {
            binding.ticketName.text = this.title
            binding.ticketImage.loadRect(this.image_url)
            binding.date.text = this.date
            binding.place.text = this.place
        }


        val offerList = getOfferList()
        adapter = OfferAdapter(offerList, offerListener)
        binding.list.adapter = adapter

        binding.back.setOnClickListener {
            finish()
        }

    }

    fun getOfferList() : List<Offer> {
        return listOf(
            Offer(
                "https://i.postimg.cc/fR50hLbk/image-test-2-1.png",
                "Obten 25% de descuento, agregando la promo de Johnnie Walker a tu compra.",
                21.2
            ),
            Offer(
                "https://i.postimg.cc/fyW6pBqV/Merch-1-1.png",
                "Obten 25% de descuento, agregando la promo de Johnnie Walker a tu compra.",
                15.1

            )
        )
    }

    private val offerListener = object : OfferAdapter.OfferListener {
        override fun onClick(offer: Offer) {
            CheckoutBottomSheet(offer).show(supportFragmentManager, null)
        }
    }
}