package com.payco.luka_app.ui.checkout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.payco.luka_app.databinding.ActivityCheckoutBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CheckoutActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCheckoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckoutBinding.inflate(layoutInflater).also {
            setContentView(it.root)
        }


    }
}