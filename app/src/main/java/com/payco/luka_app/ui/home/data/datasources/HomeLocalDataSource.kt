package com.payco.luka_app.ui.home.data.datasources

import com.payco.luka_app.ui.home.domain.models.Ticket

interface HomeLocalDataSource {
    fun getTickets() : List<Ticket>
}