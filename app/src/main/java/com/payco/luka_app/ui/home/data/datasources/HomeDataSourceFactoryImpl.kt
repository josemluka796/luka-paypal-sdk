package com.payco.luka_app.ui.home.data.datasources

import javax.inject.Inject

class HomeDataSourceFactoryImpl @Inject constructor(private val localDataSource: HomeLocalDataSource) :
    HomeDataSourceFactory {
    override fun getLocal(): HomeLocalDataSource =
        localDataSource
}