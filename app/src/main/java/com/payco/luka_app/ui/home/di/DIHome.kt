package com.payco.luka_app.ui.home.di


import com.payco.luka_app.ui.home.data.datasources.HomeDataSourceFactory
import com.payco.luka_app.ui.home.data.datasources.HomeDataSourceFactoryImpl
import com.payco.luka_app.ui.home.data.datasources.HomeLocalDataSource
import com.payco.luka_app.ui.home.data.datasources.HomeLocalDataSourceImpl
import com.payco.luka_app.ui.home.data.repository.HomeRepository
import com.payco.luka_app.ui.home.data.repository.HomeRepositoryImpl
import com.payco.luka_app.ui.home.domain.usecases.GetTicketsUseCase
import com.payco.luka_app.ui.home.domain.usecases.HomeUseCaseFactory
import com.payco.luka_app.ui.home.domain.usecases.HomeUseCaseFactoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
class DIHome {

    @Provides
    fun providesHomeLocalDataSource() : HomeLocalDataSource =
        HomeLocalDataSourceImpl()

    @Provides
    fun provideHomeDataSource(localDataSource: HomeLocalDataSource) : HomeDataSourceFactory =
        HomeDataSourceFactoryImpl(localDataSource)

    @Provides
    fun provideHomeRepository(dataSourceFactory: HomeDataSourceFactory) : HomeRepository =
        HomeRepositoryImpl(dataSourceFactory)

    @Provides
    fun provideGetTicketsUseCase(repository: HomeRepository): GetTicketsUseCase =
        GetTicketsUseCase(repository)

    @Provides
    fun provideHomeUseCaseFactory(getTicketsUseCase: GetTicketsUseCase) : HomeUseCaseFactory =
        HomeUseCaseFactoryImpl(getTicketsUseCase)

}