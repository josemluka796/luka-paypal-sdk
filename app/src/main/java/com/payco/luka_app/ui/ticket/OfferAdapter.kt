package com.payco.luka_app.ui.ticket

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.payco.luka_app.databinding.ItemProductsBinding
import com.payco.luka_app.utils.loadRect

class OfferAdapter (val list: List<Offer>, val listener: OfferListener? = null ): RecyclerView.Adapter<OfferAdapter.OfferViewHolder>() {

    class OfferViewHolder (val binding: ItemProductsBinding, val listener: OfferListener? = null ): RecyclerView.ViewHolder(binding.root) {
        fun bind(offer: Offer) {
            binding.description.text = offer.description
            binding.price.text = "$ ${offer.price}"
            binding.productImage.loadRect(offer.image_url)

            binding.buy.setOnClickListener {
                listener?.onClick(offer)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferViewHolder =
        OfferViewHolder(
            ItemProductsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            listener
        )

    override fun onBindViewHolder(holder: OfferViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size


    interface OfferListener {
        fun onClick(offer: Offer)
    }
}