package com.payco.luka_app.ui.home.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.payco.luka_app.databinding.ItemTicketBinding
import com.payco.luka_app.ui.home.domain.models.Ticket
import com.payco.luka_app.utils.loadRect

class TicketAdapter (val list: List<Ticket>, val listener: TicketListener? = null ): RecyclerView.Adapter<TicketAdapter.TicketViewHolder>() {

    class TicketViewHolder (val binding: ItemTicketBinding, val listener: TicketListener? = null ): RecyclerView.ViewHolder(binding.root) {
        fun bind(ticket: Ticket) {
            binding.ticketName.text = ticket.title
            binding.date.text = ticket.date
            binding.place.text = ticket.place
            binding.qty.text = "${ticket.quantity} Tickets"
            binding.ticketImage.loadRect(ticket.image_url)

            binding.root.setOnClickListener {
                listener?.onClick(ticket)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketViewHolder =
        TicketViewHolder(
            ItemTicketBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            listener
        )

    override fun onBindViewHolder(holder: TicketViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size


    interface TicketListener {
        fun onClick(ticket: Ticket)
    }
}