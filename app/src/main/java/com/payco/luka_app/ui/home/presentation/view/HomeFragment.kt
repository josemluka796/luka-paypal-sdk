package com.payco.luka_app.ui.home.presentation.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.payco.luka_app.databinding.FragmentHomeBinding
import com.payco.luka_app.ui.home.domain.models.Ticket
import com.payco.luka_app.ui.home.presentation.view.adapter.TicketAdapter
import com.payco.luka_app.ui.home.presentation.viewmodel.HomeViewModel
import com.payco.luka_app.ui.ticket.TicketDetailActivity
import com.payco.luka_app.utils.loadCircle
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val viewModel by viewModels<HomeViewModel>()
    private lateinit var adapter: TicketAdapter

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.avatar.loadCircle("https://i.postimg.cc/dVFCSNHK/Ellipse-8.png")
        lifecycleScope.launchWhenStarted {
            viewModel.homeState.collect {
                when(it) {
                    is HomeViewModel.HomeUiState.Empty -> {
                        showEmpty()
                    }

                    is HomeViewModel.HomeUiState.ShowTickets -> {
                        showTickets(it.tickets)
                    }
                }
            }
        }
    }

    private fun showEmpty() {

    }

    private fun showTickets(tickets: List<Ticket>) {
        adapter = TicketAdapter(tickets,ticketListener)
        binding.ticketList.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private val ticketListener = object : TicketAdapter.TicketListener {
        override fun onClick(ticket: Ticket) {
            // redirect to activity
            val intent = Intent(requireActivity(), TicketDetailActivity::class.java)
            intent.putExtra("ticket", ticket)
            startActivity(intent)
        }
    }
}