package com.payco.luka_app

import android.app.Application
import com.payco.luka.paypalsdk.Luka
import com.payco.luka.paypalsdk.data.models.LukaAuthCredentials
import com.payco.luka.paypalsdk.utils.LukaConfig
import com.payco.luka.paypalsdk.utils.LukaCurrency
import com.payco.luka.paypalsdk.utils.LukaEnvironment
import com.payco.luka.paypalsdk.utils.PaypalConfig
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Luka.initialize(
            LukaConfig.Builder()
                .setCredentials(
                    LukaAuthCredentials(
                        user = "ridery",
                        password = "short-rope-northern-quickly",
                        packageName = this.packageName
                    )
                ).build()
        )

        Luka.paypal().init(
            applicationContext,
            PaypalConfig.Builder()
                .setCurrency(LukaCurrency.USD)
                .setEnvironment(LukaEnvironment.SANDBOX)
                .isLoggingEnabled(true)
                .build()
        )
    }
}