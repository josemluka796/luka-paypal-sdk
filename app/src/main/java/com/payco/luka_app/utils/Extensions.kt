package com.payco.luka_app.utils

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.loadRect(urlImage: String?) {
    Glide.with(context)
        .load(urlImage)
        .centerCrop()
        .into(this)
}

fun ImageView.loadCircle(urlImage: String?) {
    Glide.with(context)
        .load(urlImage)
        .circleCrop()
        .into(this)
}

